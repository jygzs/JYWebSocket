<?php
error_reporting(0); // 隐藏异常
/**
 * JY WebSocket Base On Swoole
 * @author joey
 * Class JYBaseWebSocket
 */
abstract class JYBaseWebSocket
{
    /**
     * 日志记录
     * 0 不记录异常 1 记录异常主要信息 2 记录异常所有信息
     * @var int
     */
    protected $logType = 1;

    /**
     * webSocket端口
     * @var int
     */
    protected $serverPort = 9502;

    /**
     * webSocket 对象
     * @var \Swoole\WebSocket\Server|null
     */
    protected $ws = null;

    /**
     * swoole 锁对象
     * @var \Swoole\Lock|null
     */
    protected $lock = null;

    /**
     * Swoole Table object
     * @var \Swoole\Table|null
     */
    protected $swooleTable = null;


    /**
     * 初始化 swoole websocket
     * JYBaseWebSocket constructor.
     */
    public function __construct()
    {
        try {
            // swoole super table
            $this->swooleTable = new Swoole\Table(1024);
            $this->swooleTable->column('data',Swoole\Table::TYPE_STRING,2000);
            $this->swooleTable->create();
            $this->lock = new Swoole\Lock(SWOOLE_RWLOCK);
        }catch (Exception $exception)
        {
            $this->log("__construct","init swoole Table Error",$exception);
        }

        //初始化数据
        $this->setSwooleTableData();


        try {
            //创建websocket服务器对象，监听0.0.0.0:9502端口
            $this->ws = new Swoole\WebSocket\Server("0.0.0.0", $this->serverPort);
        }catch (Exception $exception)
        {
            $this->log("__construct","init swoole websocket error",$exception);
        }

        //监听WebSocket连接打开事件
        $this->ws->on('open', function ($ws, $request) {
            $this->onOpen($ws, $request);
        });
        $this->ws->on('request', function ($request, $response) {
            $this->onRequest($request, $response);
        });
        //监听WebSocket消息事件
        $this->ws->on('message', function ($ws, $frame) {
            $this->onMessage($ws, $frame);
        });
        //监听WebSocket连接关闭事件
        $this->ws->on('close', function ($ws, $fd) {
            $this->onClose($ws, $fd);
        });
        $this->ws->start();

    }

    /**
     * 监听打开连接
     * @param $ws
     * @param $request
     */
    protected function onOpen($ws, $request)
    {
        echo "client-{$request->fd} is client \n";
    }

    /**
     * 监听消息方法
     * @param Swoole\Websocket\Server $ws
     * @param Swoole\Websocket\Frame $frame
     */
    protected function onMessage($ws, $frame)
    {
        echo "Message: {$frame->data}\n";
        $ws->push($frame->fd, "server: {$frame->data}");
    }

    /**
     * 监听关闭连接
     * @param $ws
     * @param $fd
     */
    protected function onClose($ws, $fd)
    {
        echo "client-{$fd} is closed\n";
        $this->dropUser($fd);
    }

    /**
     * 监听请求
     * @param $request
     * @param $response
     */
    protected function onRequest($request, $response)
    {
        var_dump($request);
    }

    /**
     * 日志记录 可重写
     * @param string $title
     * @param string $msg
     * @param bool|Exception $exception
     */
    protected function log($title = "未知",$msg,$exception = false):void
    {
        $day = date("Y-m-d");
        if ($this->logType != 0)
        {
            $myfile = fopen("log/jy-swoole-{$day}.log", "a+");
            if ($exception === false)
            {
                $exception = "无";
            }else{
                if ($this->logType == 1)
                {
                    $exception = $exception->getMessage();
                }else if ($this->logType == 2){
                    $exception = $exception->getTraceAsString();
                }
            }

            if ($myfile === false)
            {
                echo '写入日志失败';
            }else{
                fwrite($myfile, date("Y-m-d H:i:s")."---来源：{$title} \n");
                fwrite($myfile, "信息： {$msg} \n");
                fwrite($myfile, "异常：{$exception}\n");
                fwrite($myfile, "\n");
                fclose($myfile);
            }
        }
    }

    /**
     * json封装方法
     * @param int $code
     * @param string $msg
     * @param array $data
     * @param int $count
     * @param array $diy
     * @return string
     */
    public function json($code = 0,$msg = '',$data = [],$count = 0,$diy = []):string
    {
        try {
            $response_data = [
                'code' => $code,
                'data' => $data,
                'msg' => $msg,
                'count' => $count,
            ];
            $res = array_merge($response_data,$diy);
            return json_encode($res);
        }catch (Exception $exception)
        {
            $this->log("json","get json error",$exception);
        }

    }

    /**
     * fd列表
     * @var array
     */
    protected $fdList = [];

    /**
     * 令牌列表
     * @var array
     */
    private $tokenList = [];

    /**
     * 待发送数据集合
     * @var array
     */
    private $tokenToSendMap = []; // token => []

    /**
     * 获取Swoole Table 中的数据
     * @notice 注意 本方法上了只读锁 请勿随意调用
     * @notice 要调用请配合 setSwooleTableData 使用 或者自行解锁操作
     */
    private function getSwooleTableData():void
    {
        try {
            $this->lock->lock_read();
            $e = $this->swooleTable->get('fdList');
            if ($e != false)
            {
                $this->fdList = json_decode($e["data"]);
            }
            $e = $this->swooleTable->get('tokenList');
            if ($e != false)
            {
                $this->tokenList = json_decode($e["data"]);
            }
            $e = $this->swooleTable->get('tokenToSendMap');
            if ($e != false)
            {
                $this->tokenToSendMap = json_decode($e["data"]);
            }
        }catch (Exception $exception)
        {
            $this->log("getSwooleTableData","get data from swoole table error",$exception);
        }

    }

    /**
     * 写入数据到 Swoole Table
     * @return bool
     */
    private function setSwooleTableData():bool
    {
        try {
            $rFdList = $this->swooleTable->set('fdList',["data"=>json_encode($this->fdList)]);
            $rTokenList = $this->swooleTable->set('tokenList',["data"=>json_encode($this->tokenList)]);
            $rTokenToSendMap = $this->swooleTable->set('tokenToSendMap',["data"=>json_encode($this->tokenToSendMap)]);
            $this->lock->unlock();
            if ($rFdList and $rTokenList and $rTokenToSendMap)
            {
                return true;
            }else{
                $this->log("setSwooleTableData","save to swoole table error rFd-{$rFdList}|rToken-{$rTokenList}|rTokenToSendMap-{$rTokenToSendMap}");
                return false;
            }
        }catch (Exception $exception)
        {
            $this->log("setSwooleTableData","获取swoole table数据异常",$exception);
            return false;
        }
    }

    /**
     * 保存用户
     * @param int $fd
     * @param string $token
     * @return bool
     */
    protected function setUser($fd,$token)
    {
        try {
            $this->getSwooleTableData();
            $index = array_search($fd,$this->fdList);
            $index1 = array_search($token,$this->tokenList);
            if ($index === true or $index1 === true)
            {
                return true;
            }
            array_push($this->tokenList,$token);
            array_push($this->fdList,$fd);
            $this->setSwooleTableData();
        }catch (Exception $e)
        {
            $this->log("setUser","写入用户异常",$e);
            return false;
        }

    }

    /**
     * 删除用户
     * @param int $fd
     * @return bool
     */
    protected function dropUser($fd):bool
    {
        try {
            $this->getSwooleTableData();
            $index = array_search($fd,$this->fdList);
            if ($index === false or !key_exists($index,$this->tokenList))
            {
                return false;
            }
            array_splice($this->tokenList,$index,1);
            array_splice($this->fdList,$index,1);
            $this->setSwooleTableData();
            return true;
        }catch (Exception $exception)
        {
            return false;
        }

    }

    /**
     * 通过id获取token
     * @param int $fd
     * @return bool|string
     */
    protected function getTokenByFd($fd)
    {
        $this->getSwooleTableData();
        $res = array_search($fd,$this->fdList);
        if ($res === false)
        {
            return false;
        }else{
            return $this->tokenList[$res];
        }
    }

    /**
     * 通过token获取fd
     * @param string $token 令牌
     * @return bool|int
     */
    protected function getFdByToken($token)
    {
        $this->getSwooleTableData();
        $res = array_search($token,$this->tokenList);
        if ($res === false)
        {
            return false;
        }else{
            return $this->fdList[$res];
        }
    }

    /**
     * 获取用户数量
     * @return int
     */
    protected function getUserCount() : int
    {
        $this->getSwooleTableData();
        $this->lock->unlock();
        return count($this->fdList);
    }

    /**
     * 写入待发送队列
     * @param string $token
     * @param string $data
     */
    protected function setTokenToSendMap($token,$data)
    {
        if (!in_array($token,$this->tokenToSendMap))
        {
            $this->tokenToSendMap[$token] = [$data];
        }else{
            array_push($this->tokenToSendMap[$token],$data);
        }
    }

    /**
     * 从Swoole Table获取数据
     * 本方法自带解锁 可随意调用
     */
    protected function justGet()
    {
        $this->getSwooleTableData();
        $this->lock->unlock();
    }
}