<?php

include "JYBaseWebSocket.php";
/**
 * JYFrame websocket class base on Swoole
 * @author joey
 * Class JYWebSocket
 */
class JYWebSocket extends JYBaseWebSocket
{
    private static $test = 0;

    private $toSend = [];

    const allowPushIps = ['127.0.0.1'];
    const pushToken = [];

    public function __construct()
    {
        parent::__construct();
    }

    protected function onMessage($ws, $frame)
    {
        $data = json_decode($frame->data,true);
        if (key_exists('token',$data))
        {
            $this->setUser($frame->fd,$data["token"]);
            echo "用户 {$frame->fd} 已注册";
        }
    }

    protected function onRequest($request, $response)
    {
        $response->header('content-type','application/json');
        if (!in_array($request->server["remote_addr"],self::allowPushIps))
        {
            $response->end($this->json(401,'无权操作'));
            return;
        }
        try {
            $post = $request->post;
            switch ($post["action_type"])
            {
                // 查询在线人数
                case "0":{
                    $response->end($this->json(0,'查询成功',["online_user_count"=>$this->getUserCount()]));
                    return;
                }
                case "1":{
                    $this->justGet();
                    foreach ($this->fdList as $fd)
                    {
                        if($this->ws->push($fd,$post["push_data"]))
                        {
                            continue;
                        }else{
                            $response->end($this->json(1,'发送失败'));
                            return;
                        }
                    }
                    $response->end($this->json(0,'发送成功'));
                    return;
                }
                case "2":{
                    echo '发送数据';
                    $token = $post["push_token"];
                    $fd = $this->getFdByToken($token);
                    if ($fd === false)
                    {
                        $this->setTokenToSendMap($token,$post["push_data"]);
                        $response->end($this->json(1,'该用户不在线，已添加发送队列'));
                    }
                    if($this->ws->push($fd,$post["push_data"]))
                    {
                        $response->end($this->json(0,'发送成功'));
                    }else{
                        $response->end($this->json(1,'发送失败'));
                        return;
                    }
                }
            }

        }catch (Exception $e)
        {
            $response->end($this->json(1,'参数有误'));
            return;
        }
    }
}
new JYWebSocket();