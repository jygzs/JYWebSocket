# JYWebSocket

#### 介绍
本项目基于swoole4开发，集成相关常用功能，多线程，集成swoole table解决共享变量问题，内置用户系统，token fd双转换，可直接使用，包含 异常 处理，日志记录。


#### 软件架构
php面向对象编程 集成包含Swoole4的php，下载后可直接运行


#### 安装教程


**windows用户只需克隆项目至本地**，**双击run.bat即可开启websocket服务器**
	
	cd 你想要放置的路径
	git clone https://gitee.com/jygzs/JYWebSocket.git

`克隆到本地后请在JYWebSocket.php中编写相关业务代码`



`JYWebSocket常用功能说明` `所有方法、变量都可以在JYWebSocket.php中调用、重写（部分不建议重写）`

	/**
     * 日志记录
     * 0 不记录异常 1 记录异常主要信息 2 记录异常所有信息
     * @var int
     */
    protected $logType = 1;
	// 建议重写

    /**
     * webSocket端口
     * @var int
     */
    protected $serverPort = 9502;
	// 建议重写 可修改websocket端口
	
	protected $fdList = []; 
	// fd列表 在读取之前请调用justGet更新数据
	// 不建议重写

	protected function onOpen($ws, $request)
	// 监听打开连接
	// 建议重写

	protected function onMessage($ws, $frame)
	// 监听消息事件  （客户端发过来的）
	// 建议重写

	protected function onClose($ws, $fd)
	// 监听关闭连接
	// 建议重写

	protected function onRequest($request, $response)
	// 监听请求
	// Http服务器 一般用来接收后端发送的请求将数据推送到客户端上
	// 建议重写

	protected function log($title = "未知",$msg,$exception = false):void 
	// 日志记录
	// 可被重写 默认日志记录在log文件夹下 按天记录

	public function json($code = 0,$msg = '',$data = [],$count = 0,$diy = []):string 
	// 用以封装返回json

	protected function setUser($fd,$token) 
	// 保存用户
	// 不建议重写

	protected function dropUser($fd):bool 
	// 移除用户
	// 不建议重写

	protected function getTokenByFd($fd) 
	// 通过id获取token
	// 不建议重写

	protected function getFdByToken($token) 
	// 通过token获取fd
	// 不建议重写

	protected function getUserCount() : int 
	// 获取用户数量 setUser保存的用户数量
	// 不建议重写

	protected function setTokenToSendMap($token,$data) :void 
	// 将发送失败(用户不在线) 数据写入到待发送队列
	// 不建议重写

	protected function justGet() 
	// 更新数据
	// 不建议重写
### [项目地址 JYWebsocket码云地址](https://gitee.com/jygzs/JYWebSocket)
### [Swoole官方文档 Swoole4文档](https://wiki.swoole.com/)

#### 使用说明

1.  请在JYWebSocket.php中编辑代码 最好别动JYBaseWebSocket
2.  也可自行建类，继承JYBaseWebSocket就可以使用，但请修改run.bat中的启动类

#### 参与贡献

1.  鸣谢Swoole4
2.  Swoole4官方文档 `https://wiki.swoole.com/`


